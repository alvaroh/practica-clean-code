package asf.cleanCode.generatingPrimes;

import java.util.ArrayList;
import java.util.List;

/**
 * Esta clase genera números primos hasta un máximo dado.
 * Se usa como algoritmo la criba de Eratóstenes
 * <p>
 * Eratóstenes de Cirene (en griego antiguo Ἐρατοσθένης, Eratosthénēs)
 * (Cirene, 276 a. C.1​-Alejandría, 194 a. C.) fue un matemático,
 * astrónomo y geógrafo griego de origen cirenaico.
 * Fue la primera persona en calcular la circunferencia de la Tierra
 * <p>
 * Se forma una tabla con todos los números naturales comprendidos entre
 * 2 y n, y se van tachando los números que no son primos de la siguiente
 * manera: Comenzando por el 2, se tachan todos sus múltiplos; comenzando
 * de nuevo, cuando se encuentra un número entero que no ha sido tachado,
 * ese número es declarado primo, y se procede a tachar todos sus múltiplos,
 * así sucesivamente. El proceso termina cuando el cuadrado del siguiente
 * número confirmado como primo es mayor que n
 *
 * @author Mikel
 * @version 13 Ago 2019
 */

public class PrimosClass
{
  /**
   * @param maximo es el límite de la sucesión de primos
   */
  public static List<Integer> generadorDePrimos(int maximo)
  {
    if (maximo >= 2) // el único caso válido
    {
      // declaraciones
      int t = maximo + 1; // tamaño del array
      boolean[] booleanos = new boolean[t];
      int i;
      // inicialización del array a true
      for (i = 0; i < t; i++)
        booleanos[i] = true;

      // Nos deshacemos de los primos conocidos
      booleanos[0] = booleanos[1] = false;

      // criba
      int j;
      for (i = 2; i < Math.sqrt(t) + 1; i++)
      {
        if (booleanos[i]) // si i no está cruzado, cruzamos con sus múltiplos
        {
          for (j = 2 * i; j < t; j += i)
            booleanos[j] = false; // el múltiplo no es primo
        }
      }

      // ¿cuántos primos hay?
      int contador = 0;
      for (i = 0; i < t; i++)
      {
        if (booleanos[i])
          contador++; // contador de éxitos
      }

      List<Integer> primos = new ArrayList<>();
      //int[] primos = new int[contador];

      // movemos los primos al resultado
      for (i = 0, j = 0; i < t; i++)
      {
        if (booleanos[i])             // si es primo...
          primos.add(j++,i);
      }

      return primos; // devolvemos los primos
    }
    else // maximo < 2
    return null; // devolvemos null
  }

}