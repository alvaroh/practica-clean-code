package asf.cleanCode.pintaPrimos.original;

public class pintaPrimos {

  public static final String ESPACIO = "";
  public static final String SALTODEPAGINA = "\f";
  public static final int CANTIDADDEPRIMOSMAXIMA = 1000;
  public static final int LIN = 50;
  public static final int NUMERODECOLUMNAS = 4;
  public static final int ORDMAX = 30;

  public static void pinta() {

    int listaDePrimos[] = new int[CANTIDADDEPRIMOSMAXIMA + 1];

    calcularListaDePrimos(listaDePrimos);

    printPaginasDePrimos(listaDePrimos);

  }

  private static void calcularListaDePrimos(int[] listaDePrimos) {
    Parametros parametros = new Parametros();
    listaDePrimos[1] = 2;

    while (parametros.getCantidadActualDePrimos() < CANTIDADDEPRIMOSMAXIMA) {
      calcularPrimo(listaDePrimos, parametros);
      actualizarListaDePrimos(parametros, listaDePrimos);
    }
  }

  private static void calcularPrimo(int[] listaDePrimos, Parametros parametros) {
    boolean esPrimo;

    do {
      parametros.setNumeroActual(parametros.getNumeroActual() + 2);
      eliminarPosiblesPrimos(listaDePrimos, parametros);
      esPrimo = comprobarSiEsPrimo(parametros, listaDePrimos);
    } while (!esPrimo);
  }

  private static void eliminarPosiblesPrimos(int[] listaDePrimos, Parametros parametros) {
    if (parametros.getNumeroActual() == parametros.getCuadrado()) {
      parametros.setOrd(parametros.getOrd() + 1);
      parametros.setCuadrado(listaDePrimos[parametros.getOrd()] * listaDePrimos[parametros.getOrd()]);
      actualizarMult(parametros);
    }
  }

  private static void actualizarMult(Parametros parametros) {
    int[] multActual = parametros.getMult();

    multActual[parametros.getOrd() - 1] = parametros.getNumeroActual();
    parametros.setMult(multActual);
  }

  private static boolean comprobarSiEsPrimo(Parametros parametros, int[] listaDePrimos) {
    boolean esPrimo = true;
    int N = 2;

    while (N < parametros.getOrd()) {
      while (parametros.getMult()[N] < parametros.getNumeroActual())
        parametros.getMult()[N] = parametros.getMult()[N] + listaDePrimos[N] + listaDePrimos[N];
      if (parametros.getMult()[N] == parametros.getNumeroActual())
        esPrimo = false;
      N = N + 1;
    }
    return esPrimo;
  }

  private static void actualizarListaDePrimos(Parametros parametros, int[] listaDePrimos) {
    parametros.setCantidadActualDePrimos(parametros.getCantidadActualDePrimos() + 1);
    listaDePrimos[parametros.getCantidadActualDePrimos()] = parametros.getNumeroActual();
  }

  private static void printPaginasDePrimos(int[] listaDePrimos) {
    int NUMERODEPAGINA = 1;
    int PAGEOFFSET = 1;

    while (PAGEOFFSET <= CANTIDADDEPRIMOSMAXIMA) {
      printPagina(listaDePrimos, NUMERODEPAGINA, PAGEOFFSET);
      NUMERODEPAGINA = NUMERODEPAGINA + 1;
      PAGEOFFSET = PAGEOFFSET + LIN * NUMERODECOLUMNAS;
    }
  }

  private static void printPagina(int[] listaDePrimos, int NUMERODEPAGINA, int PAGEOFFSET) {
    printInicioDePagina(NUMERODEPAGINA);
    for (int ROWOFFSET = PAGEOFFSET; ROWOFFSET < PAGEOFFSET + LIN; ROWOFFSET++){
      printNumerosPrimos(listaDePrimos, ROWOFFSET);
    }
    System.out.println(SALTODEPAGINA);
  }

  private static void printNumerosPrimos(int[] listaDePrimos, int ROWOFFSET) {
    for (int i = 0; i < NUMERODECOLUMNAS;i++)
      if (ROWOFFSET + i * LIN <= CANTIDADDEPRIMOSMAXIMA)
        System.out.format("%10d", listaDePrimos[ROWOFFSET + i * LIN]);
    System.out.println(ESPACIO);
  }

  private static void printInicioDePagina(int NUMERODEPAGINA) {
    System.out.println("Los primeros " + CANTIDADDEPRIMOSMAXIMA + " Números primos --- Página " + NUMERODEPAGINA);
    System.out.println(ESPACIO);
  }

  private static class Parametros {
    private int numeroActual;
    private int cantidadActualDePrimos;
    private int cuadrado;
    private int ord;
    private int[] mult;

    public int getNumeroActual() {
      return numeroActual;
    }

    public int getCantidadActualDePrimos() {
      return cantidadActualDePrimos;
    }

    public int getCuadrado() {
      return cuadrado;
    }

    public int getOrd() {
      return ord;
    }

    public int[] getMult() {
      return mult;
    }

    public void setCantidadActualDePrimos(int cantidadActualDePrimos) {
      this.cantidadActualDePrimos = cantidadActualDePrimos;
    }

    public void setNumeroActual(int numeroActual) {
      this.numeroActual = numeroActual;
    }

    public void setCuadrado(int cuadrado) {
      this.cuadrado = cuadrado;
    }

    public void setOrd(int ord) {
      this.ord = ord;
    }

    public void setMult(int[] mult) {
      this.mult = mult;
    }

    public Parametros(){
      numeroActual = 1;
      cantidadActualDePrimos = 1;
      cuadrado = 9;
      ord = 2;
      mult = new int[ORDMAX + 1];
    }
  }
}