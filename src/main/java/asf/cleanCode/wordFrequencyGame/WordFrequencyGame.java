package asf.cleanCode.wordFrequencyGame;

import java.util.*;

public class WordFrequencyGame {

    public static final String DELIMITER = "\n";
    public static final String SEPARATOR = " ";
    public static final String SPLIT_SEPARATOR = "\\s+";
    public static final int INITIALVALUE = 1;

    public String getResult(String cadenaDeEntrada) {
        if (tieneSoloUnaPalabra(cadenaDeEntrada)) {
            return cadenaDeEntrada + " 1";
        } else {
            return calcularResultadoParaCadenaConVariasPalabras(cadenaDeEntrada);
        }
    }

    private String calcularResultadoParaCadenaConVariasPalabras(String cadenaDeEntrada) {
        try {
            List<Word> listaDePalabras = obtenerListaDePalabras(cadenaDeEntrada);
            Map<String, List<Word>> map = obtenerMapaDeFrecuencias(listaDePalabras);
            listaDePalabras = obtenerListaDePalabrasConFrecuencias(map);
            ordenarListaPorFrecuencia(listaDePalabras);
            String cadenaResultado = obtenerCadenaResultado(listaDePalabras);
            return cadenaResultado;
        } catch (Exception e) {
            return "Calculate Error";
        }
    }

    private String obtenerCadenaResultado(List<Word> listaDePalabras) {
        StringJoiner stringJoiner = new StringJoiner(DELIMITER);
        for (Word word : listaDePalabras) {
            String s = word.getValue() + SEPARATOR + word.getWordCount();
            stringJoiner.add(s);
        }
        return stringJoiner.toString();
    }

    private void ordenarListaPorFrecuencia(List<Word> listaDePalabras) {
        listaDePalabras.sort((word1, word2) -> word2.getWordCount() - word1.getWordCount());
    }

    private List<Word> obtenerListaDePalabrasConFrecuencias(Map<String, List<Word>> map) {
        List<Word> listaDePalabras;
        List<Word> listaAuxiliar = new ArrayList<>();
        for (Map.Entry<String, List<Word>> entry : map.entrySet()) {
            Word word = new Word(entry.getKey(), entry.getValue().size());
            listaAuxiliar.add(word);
        }
        listaDePalabras = listaAuxiliar;
        return listaDePalabras;
    }

    private Map<String, List<Word>> obtenerMapaDeFrecuencias(List<Word> listaDePalabras) {
        Map<String, List<Word>> resultado = new HashMap<>();
        for (Word word : listaDePalabras) {
            contabilizarPresenciaPalabra(resultado, word);
        }
        return resultado;
    }

    private void contabilizarPresenciaPalabra(Map<String, List<Word>> map, Word word) {
        if (palabraEstaPresenteEnMapa(map, word)) {
            ArrayList arrayList = new ArrayList<>();
            arrayList.add(word);
            map.put(word.getValue(), arrayList);
        } else {
            map.get(word.getValue()).add(word);
        }
    }

    private boolean palabraEstaPresenteEnMapa(Map<String, List<Word>> resultado, Word word) {
        return !resultado.containsKey(word.getValue());
    }

    private List<Word> obtenerListaDePalabras(String inputString) {

        String[] palabras = inputString.split(SPLIT_SEPARATOR);
        List<Word> wordList = new ArrayList<>();
        for (String string : palabras) {
            Word word = new Word(string, INITIALVALUE);
            wordList.add(word);
        }
        return wordList;
    }

    private boolean tieneSoloUnaPalabra(String inputStr) {
        return inputStr.split(SPLIT_SEPARATOR).length == 1;
    }
}
