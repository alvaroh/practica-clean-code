package asf.cleanCode.pintaPrimos.original;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.util.Scanner;

import static org.junit.Assert.*;

public class pintaPrimosTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void pinta() throws Exception{

        File file = new File("src/test/java/asf/cleanCode/pintaPrimos/original/resultado.txt");
        String content = new Scanner(file).useDelimiter("\\Z").next().replaceAll("\n", "").replaceAll("\r", "");


        pintaPrimos.pinta();
        String result = outContent.toString().replaceAll("\n", "").replaceAll("\r", "");


        assertEquals(result, content);
    }

}